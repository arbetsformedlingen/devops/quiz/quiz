FROM docker.io/library/nginx:1.27.4

# support running as arbitrary user which belogs to the root group
RUN chmod g+rwx /var/cache/nginx /var/run /var/log/nginx

EXPOSE 8081

RUN    ln -sf /dev/stdout       /var/log/nginx/nginx-access.log \
       && ln -sf /dev/stderr    /var/log/nginx/nginx-error.log

STOPSIGNAL SIGQUIT

COPY ./default.conf /etc/nginx/conf.d
COPY ./index.html /usr/share/nginx/html/

CMD [ "nginx", "-g", "daemon off; error_log stderr info;"]
